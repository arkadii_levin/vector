#include <iostream>

using namespace std;

class Vector
{
public:
	enum class ErrorCode {
		pointer_is_nullptr,
		ok
	};

	void printErrorCode(ErrorCode code) {
		switch (code)
		{
		case ErrorCode::pointer_is_nullptr:
			cout << "Was recieved pointer to null data.\n";
			break;
		case ErrorCode::ok:
			cout << "All ok. No any errors.\n";
			break;
		default:
			break;
		}
	}

	void checkForError(ErrorCode code) {
		if (code != ErrorCode::ok)
		{
			printErrorCode(code);
		}
	}

	int intErrorCheck() {
		int value = 0;

		while (!(cin >> value) || value < 0) {
			cin.clear();
			cin.ignore(std::numeric_limits< streamsize >::max(), '\n');
			cout << "Please enter only positive number!" << endl;
		}

		return value;
	}

	void getSizeFromConsole() {
		cout << "Please enter array size: ";

		size = intErrorCheck();
	}

	void createArray() {
		pointer = new int[size];
	}

	ErrorCode fillArray() {
		if (!pointer) {
			return ErrorCode::pointer_is_nullptr;
		}

		srand(time(0));
		for (size_t i = 0u; i < size; i++) {
			pointer[i] = rand() % 100;
		}

		return ErrorCode::ok;
	}

	void outputArray() {
		cout << "\nArray elements: ";

		for (size_t i = 0u; i < size; i++) {
			cout << pointer[i] << " ";
		}

		cout << endl << endl;
	}

	void pushFront() {
		int* push_front_pointer = new int[size + 1];
		push_front_pointer[0] = intErrorCheck();

		for (size_t i = 1u, j = 0u; i < size + 1; i++, j++) {
			push_front_pointer[i] = pointer[j];
		}

		size++;
		delete[] pointer;
		pointer = push_front_pointer;
	}

	void pushBack() {
		int* push_back_pointer = new int[size + 1];

		for (size_t i = 0u; i < size; i++) {
			push_back_pointer[i] = pointer[i];
		}

		push_back_pointer[size] = intErrorCheck();

		size++;
		delete[] pointer;
		pointer = push_back_pointer;
	}

	void popFront() {
		int* pop_front_pointer = new int[size];

		for (size_t i = 0u, j = 1u; i < size; i++, j++) {
			pop_front_pointer[i] = pointer[j];
		}

		size--;
		delete[] pointer;
		pointer = pop_front_pointer;
	}

	void popBack() {
		int* pop_back_pointer = new int[size];

		for (size_t i = 0u; i < size - 1; i++) {
			pop_back_pointer[i] = pointer[i];
		}

		size--;
		delete[] pointer;
		pointer = pop_back_pointer;
	}

	string getStringFromConsole() {
		string answer;

		cin >> answer;
		return answer;
	}

	void answerAddEffect() {
		string answer = getStringFromConsole();
		cout << "Enter the element: ";

		if (answer == "beg") {
			pushFront();
		}
		else if (answer == "end") {
			pushBack();
		}
	}

	void answerDelEffect() {
		string answer = getStringFromConsole();

		if (answer == "beg") {
			popFront();
		}
		else if (answer == "end") {
			popBack();
		}
	}

	void questionPushOrPop() {
		cout << "Do you want to add or delete the element? 'add' or 'del'" << endl;
		string answer = getStringFromConsole();

		if (answer == "add") {
			cout << "Do you want to add an element to the beginning or end of the array? 'beg' or 'end'" << endl;
			answerAddEffect();
		}
		else if (answer == "del") {
			cout << "Do you want to delete an element to the beginning or end of the array? 'beg' or 'end'" << endl;
			answerDelEffect();
		}

		outputArray();
	}

	void getElement() {
		cout << "Enter the element position: ";
		int position = intErrorCheck();

		cout << "Element on position " << position << " is: " << pointer[position] << endl;
	}

	void setElement() {
		cout << "\nEnter the position of the element you want to change: ";

		int position = intErrorCheck();
		cout << "Enter the element you want to change: ";

		int element = intErrorCheck();

		pointer[position] = element;
		outputArray();
	}

	void findElement() {
		cout << "Enter the element you want to find: ";

		int element = intErrorCheck();

		for (size_t i = 0u; i < size; i++) {
			if (pointer[i] == element) {
				cout << "Element " << pointer[i] << " is on position: " << i << endl;
			}
		}
	}

	void swapElemets(int i) {
		int tmp = pointer[i];
		pointer[i] = pointer[i + 1];
		pointer[i + 1] = tmp;
	}

	void sortElements() {
		for (size_t j = 0u; j < size; j++) {
			for (size_t i = 0u; i < size - 1; i++) {
				if (pointer[i] < pointer[i + 1]) {
					continue;
				}
				else {
					swapElemets(i);
				}
				outputArray();
			}
		}
	}

	void deletePointer() {
		delete[] pointer;
	}

private:
	size_t size;
	int* pointer;
};

int main() {
	Vector object;

	object.getSizeFromConsole();
	object.createArray();
	object.checkForError(object.fillArray());
	object.outputArray();
	object.questionPushOrPop();

	object.getElement();
	object.setElement();
	object.findElement();
	object.sortElements();

	object.deletePointer();
	return 0;
}